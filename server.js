const express = require("express");
const app = express();

const request = require("request");
const fs = require("fs");
const Puid = require("puid");
const _ =  require("lodash");

/// connect with DB ////
//const mongoskin = require("./config/mongoskin");

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");

const path = require("path");
app.use(express.static(path.join(__dirname, "public")));


app.get("/puid", (req, res) => {
    var puid = new Puid(false);   // parameters : blank/true/false
    res.send("Puid : " + puid.generate());
});

app.get("/lodash", (req, res) => {
    var mem = {id : 10, name : "Test", email : "abc@gmial.com"} ;
    var fdata = _.omit(mem, "name");
    res.send(fdata);
});


app.get("/", (req, res) => {

    request('https://www.google.com/logos/doodles/2018/winter-solstice-2018-northern-hemisphere-5609689915064320-law.gif').pipe(fs.createWriteStream('test.gif'));

    //res.render("test/comman");
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log("Listening port " + port);
});